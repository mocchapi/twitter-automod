import tweepy
from tweepyAuth import auto_authenticate
from time import sleep
import datetime

frequency = 15 # how often a block sweep will occur (in minutes) dont make this too low or youll get ratelimited

if __name__ == '__main__':
	api = auto_authenticate()

	try:
		with open('bannedwords.txt','r') as f:
			bannedwords = list(f.read().split('\n'))
	except:
		print('no bannedwords.txt found, making one now\nplease fill in one banned word per line')
		with open('bannedwords.txt','w') as f:
			f.write('')
		exit()

	excluded_users = []
	try:
		with open('excluded_users.txt','r') as f:
			excluded_users = list(f.read().split('\n'))
	except:
		print('no excluded_users.txt found, making one now\nplease fill in one username that shouldnt get banned per line')
		with open('excluded_users.txt','w') as f:
			f.write('')

	logtext = ''
	try:
		with open('blocked_users.txt','r') as f:
			logtext = f.read()
	except:
		pass

	me = api.me()
	print(f'{len(bannedwords)} banned words registered')
	search_words = f'((@{me.screen_name} OR twitter.com/{me.screen_name}) AND ({" OR ".join(bannedwords)})) -from:traabot'
	# print(search_words)
	while True:
		try:
			blocked_ids = api.blocks_ids()
			processed = 0
			tweets = tweepy.Cursor(api.search, q=search_words, tweet_mode="extended",include_rts=True).items(100)
			for item in tweets:
				if item.author.id not in blocked_ids and item.author.screen_name not in excluded_users:
					processed +=1
					now = datetime.datetime.now()
					api.create_block(item.author.id)
					time = now.strftime('%Y-%m-%d %H:%M:%S')
					log = f'[{time}] blocked {item.author.screen_name} ({item.author.id}) for post https://twitter.com/{item.author.screen_name}/status/{item.id_str}'
					print(log)
					logtext = f'{logtext}\n{log}'
					blocked_ids.append(item.author.id)
				else:
					now = datetime.datetime.now()
					time = now.strftime('%Y-%m-%d %H:%M:%S')
					#print(f'[{time}] user already blocked: {item.author.screen_name} ({item.author.id})')
			with open('blocked_users.txt','w') as f:
				f.write(logtext)
			now = datetime.datetime.now()
			time = now.strftime('%Y-%m-%d %H:%M:%S')
			print(f'[{time}] blocked {processed} users')
		except tweepy.error.RateLimitError:
			now = datetime.datetime.now()
			time = now.strftime('%Y-%m-%d %H:%M:%S')
			print(f'[{item}] Ratelimit hit')
		now = datetime.datetime.now()
		time = now.strftime('%Y-%m-%d %H:%M:%S')
		print(f'[{time}] sleeping for {frequency}m')
		sleep(frequency*60)