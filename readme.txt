easy to use twitter automod
it blocks users that say banned words (as defined in bannedwords.txt, one word per line) in the authenticated user's mentions

This required my tweepyAuth library (found at https://gitlab.com/mocchapi/tweepyauth)
and Tweepy (pip3 install tweepy)

launching main.py for the first time will run a setup, after that you can run the same file to run the bot

by default it waits an hour between every sweep